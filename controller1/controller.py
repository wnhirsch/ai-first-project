from controller_interface import ControllerInterface
from random import *
from math import *
from config import *

# Parâmetros do algoritmo genético
POPULATION_SIZE = 100
MUTATION_RATE = 1.0
CROSSOVER_RATE = 1.0

NUM_PART_TOURNAMENT = 4
MAX_TIME = 5
SEED_NUMBER = 0

FEATURE_MAX = 1
FEATURE_MIN = 0
NUM_FEATURES = 5
SOLUTION_SIZE = (NUM_FEATURES+1) * 5

# Calcula número de mutações e recombinações a serem feitas por geração
NUM_CROSSOVER = int(POPULATION_SIZE * CROSSOVER_RATE)
NUM_MUTATION = int(POPULATION_SIZE * MUTATION_RATE)

PERTUBATION = 0.2
TABU_LIST_SIZE = 10
MAX_ITER_WITHOUT_IMPROVEMENT = 100000

# Hill Climbing

def findNeighbors(state, k):
    neighbors = []
    for i, weight in enumerate(state):
        newState1 = state[:]
        newState1[i] = weight + k
        neighbors.append(newState1)

        newState2 = state[:]
        newState2[i] = weight - k
        neighbors.append(newState2)
    return neighbors

# Busca Tabu

def neighbors(solution, pos):
    solution1 = solution2 = None

    if (solution[pos] + PERTUBATION <= FEATURE_MAX):
        solution1 = solution[:]
        solution1[pos] += PERTUBATION

    if (solution[pos] - PERTUBATION >= FEATURE_MIN):
        solution2 = solution[:]
        solution2[pos] -= PERTUBATION

    return solution1, solution2

# Algoritmo Genetico

def generate():
    return [(randrange(0, 6)*2/10) for size in range(SOLUTION_SIZE)]

def mutate(solution):
    newSolution = solution[:]
    pos = randrange(0, SOLUTION_SIZE)
    newSolution[pos] = random()
    return newSolution

def crossover(solution1, solution2):
    pos = randrange(1, SOLUTION_SIZE-1)
    return [solution1[:pos] + solution2[pos:], solution2[:pos] + solution1[pos:]]

def tournament(population, fitness, k):
    posBestSolution = 0

    for t in range(k):
        posSelectedSolution = randrange(0, POPULATION_SIZE)

        if (fitness[posSelectedSolution] > fitness[posBestSolution]):
            posBestSolution = posSelectedSolution

    return population[posBestSolution], fitness[posBestSolution]

def selection(population, fitness):
    selectedPopulation = []
    selectedFitness = []

    for s in range(POPULATION_SIZE):
        selectedSolution, fitnessSolution = tournament(population, fitness, NUM_PART_TOURNAMENT)
        selectedPopulation.append(selectedSolution)
        selectedFitness.append(fitnessSolution)

    return selectedPopulation, selectedFitness

# Features

def tovector(p, q) -> tuple:
    # return a vector between two points

    return q[0] - p[0], q[1] - p[1]

def norm(v) -> int:
    #return the norm of a vector 2d

    return sqrt(v[0]*v[0] + v[1]*v[1])

def dist(p, q) -> int:
    # return the distance of two points

    return norm(tovector(p, q))

def getorientation(theta) -> tuple:
    # return a vector of norm 1 with the orientation of the drill

    return cos(theta), sin(theta)

def cross(a, b) -> int:
    # return the cross product of two vectors

    return a[0]*b[1] - a[1]*b[0]

def ccw(p, q, r) -> bool:
    # return true if point r is at left of straight pq

    return cross(tovector(p, q), tovector(p, r)) > 0

def rightorleft(drill_edge_position, drill_angle, target_position) -> bool:
    # return true if drill should turn right or false if should turn left

    drill_orientation = getorientation(drill_angle-(pi/2))
    r = drill_edge_position[0] + drill_orientation[0], drill_edge_position[1] + drill_orientation[1]
    return ccw(drill_edge_position, target_position, r)

def rla(drill_edge_position, drill_angle, target_position, align) -> bool:
    # return 0.5 if aligned with target else return the direction to turn
    if (align):
        return 0.5
    else:
        return rightorleft(drill_edge_position, drill_angle, target_position)

def needDischarge(cooldown, enemy_touching_asteroid) -> bool:
    # return true if the discharge should be use or false if no
    return cooldown == 0 and enemy_touching_asteroid

def needGas(drill_gas) -> bool:
    # return true if the dril have poor gas
    return drill_gas <= 50

class Controller(ControllerInterface):

    def hillClimbing(self, weights: tuple):
        k = 0.2
        actualState = [0.5] * SOLUTION_SIZE
        actualFitness = self.run_episode(tuple(actualState))
        # Critério de parada
        isLocalMax = False

        try:
           file = open("hillClimbingstatistic.csv","w")
           file.write("Iteration, Fitness\n")
           file.close()

        except:
           print("Couldn't open the file!\n")

        cont = 0

        while(True):
            while(not isLocalMax):
                print("Current Best Solution: {}\nFitness: {}\n".format(actualState, actualFitness))
                cont = cont + 1
                try:
                   file = open("hillClimbingstatistic.csv", "a")
                   file.write("{}, {}\n".format(cont, actualFitness))
                   file.close()
                except:
                   print("Couldn't append the file!\n")
                # Compute neighbors states
                neighborsList = findNeighbors(actualState, k)
                bestNeighbor = actualState
                bestFitness = actualFitness
                # For each neighbor...
                for neighbor in neighborsList:
                    # Compute your fitness value
                    neighborFit = self.run_episode(tuple(neighbor))
                    # And compare with the actual best solution
                    if neighborFit > bestFitness:
                        bestFitness = neighborFit
                        bestSolution = neighbor
                # If the best state is better than the actual state
                if bestFitness > actualFitness:
                    actualState = bestSolution
                    actualFitness = bestFitness
                else:
                    isLocalMax = True
            print("*** Current Best Solution WITH K = {}: {}\nFitness: {}\n".format(k, actualState, actualFitness))
            k = k / 2
            isLocalMax = False

        return tuple(actualState)

    def tabuSearch(self, weights: tuple):
        iterationWithoutImprovement = 0
        # currentSolution = list(weights)
        currentSolution = [0.5] * SOLUTION_SIZE
        fitnessCurrentSolution = self.run_episode(tuple(currentSolution))

        tabuList = []
        currentListSolution = []
        currentListFitness = []

        bestSolution = currentSolution[:]
        fitnessBestSolution = fitnessCurrentSolution

        try:
           file = open("tabuSearchstatistic.csv","w")
           file.write("Iteration, Fitness\n")
           file.close()

        except:
           print("Couldn't open the file!\n")

        cont = 0

        while(iterationWithoutImprovement <= MAX_ITER_WITHOUT_IMPROVEMENT):
            for pos in range(SOLUTION_SIZE):
                solution1, solution2 = neighbors(currentSolution, pos)

                if (solution1 != None and solution1 not in tabuList):
                    currentListSolution.append(solution1)

                if (solution2 != None and solution2 not in tabuList):
                    currentListSolution.append(solution2)

            # Para forçar que o solução atual seja algum dos vizinhos atais
            fitnessCurrentSolution = -1

            for solution in currentListSolution:
                fitness = self.run_episode(tuple(solution))

                if (fitness > fitnessCurrentSolution):
                    currentSolution = solution[:]
                    fitnessCurrentSolution = fitness

            if (fitnessCurrentSolution > fitnessBestSolution):
                    bestSolution = currentSolution[:]
                    fitnessBestSolution = fitnessCurrentSolution

            # Mantém o tamanho máximo da lista tabu
            tabuList.append(currentSolution)

            if (len(tabuList) > TABU_LIST_SIZE):
                tabuList.pop(0)

            currentListSolution = []
            iterationWithoutImprovement += 1
            print("Current Best Solution: {}\nFitness: {}\n".format(bestSolution, fitnessBestSolution))

            cont = cont + 1

            try:
               file = open("tabuSearchstatistic.csv", "a")
               file.write("{}, {}\n".format(cont, fitnessBestSolution))
               file.close()
            except:
               print("Couldn't append the file!\n")

        return tuple(bestSolution)

    def geneticAlgorithm(self, weights: tuple):
        # Estruturas de Dados
        population = []
        fitness = []

        bestSolution = []
        fitnessBestSolution = 0
        genWithoutImprovement = 0

        try:
           file = open("geneticstatistic.csv","w")
           file.write("Iteration, Fitness\n")
           file.close()

        except:
           print("Couldn't open the file!\n")

        cont = 0

        # Gerar população inicial
        for i in range(POPULATION_SIZE):
            newSolution = generate()
            print(newSolution)
            population.append(newSolution)
            fitness.append(self.run_episode(tuple(newSolution)))

        while(genWithoutImprovement <= MAX_ITER_WITHOUT_IMPROVEMENT):
            # Crossover
            for i in range(NUM_CROSSOVER):
                solutionA, fitnessA = tournament(population, fitness, NUM_PART_TOURNAMENT)
                solutionB, fitnessB = tournament(population, fitness, NUM_PART_TOURNAMENT)

                newSolution1, newSolution2 = crossover(solutionA, solutionB)

                population.append(newSolution1)
                fitness.append(self.run_episode(tuple(newSolution1)))
                population.append(newSolution2)
                fitness.append(self.run_episode(tuple(newSolution2)))

            # Mutation
            for i in range(NUM_MUTATION):
                pos = randrange(0, SOLUTION_SIZE)
                solution1, solution2 = neighbors(choice(population), pos)

                if (solution1 != None):
                    population.append(solution1)
                    fitness.append(self.run_episode(tuple(solution1)))

                if (solution2 != None):
                    population.append(solution2)
                    fitness.append(self.run_episode(tuple(solution2)))

            # Verifica se existe uma solução melhor na população atual do que a melhorjá registrada anteriormente
            for i in range(len(population)):
                if (fitness[i] > fitnessBestSolution):
                    bestSolution = population[i]
                    fitnessBestSolution = fitness[i]
                    genWithoutImprovement = 0

            genWithoutImprovement += 1
            print("Current Best Solution: {}\nFitness: {}\n".format(bestSolution, fitnessBestSolution))

            cont = cont + 1

            try:
               file = open("geneticstatistic.csv", "a")
               file.write("{}, {}\n".format(cont, fitnessBestSolution))
               file.close()
            except:
               print("Couldn't append the file!\n")

            # Seleção
            population, fitness = selection(population, fitness)

        return tuple(bestSolution)

    def take_action(self, weights: tuple) -> int:
        """
        :return: An integer corresponding to an action:
        1 - Right
        2 - Left
        3 - Accelerate forward
        4 - Discharge
        5 - Nothing
        """

        features = Controller.compute_features(self, self.sensors)
        num_features = len(features)

        action = [weights[0], weights[num_features+1], weights[2*(num_features+1)], weights[3*(num_features+1)], weights[4*(num_features+1)]]

        for i in range(num_features):
            action[0] += weights[i+1] * features[i]
            action[1] += weights[(num_features+1)+i+1] * features[i]
            action[2] += weights[2*(num_features+1)+i+1] * features[i]
            action[3] += weights[3*(num_features+1)+i+1] * features[i]
            action[4] += weights[4*(num_features+1)+i+1] * features[i]

        return action.index(max(action)) + 1

    def compute_features(self, sensors: dict) -> tuple:
        """
        This function should take the raw sensor information of the ship (see below) and compute useful features
        for selecting an action.
        The ship has the following sensors:

        :param sensors: contains:
             'asteroid_position': (x, y)
             'asteroid_velocity': (n, m)
             'asteroid_resources': 0 - ???
             'align_asteroid': 0 or 1
             'align_mothership': 0 or 1
             'drill_angle': angle in rad
             'drill_position': (x, y)
             'drill_velocity': (n, m)
             'drill_mothership_position': (x, y)
             'drill_resources': 0 - ???
             'drill_touching_asteroid': 0 or 1
             'drill_touching_mothership': 0 or 1
             'drill_discharge_cooldown': 0 - COOLDOWN (defined in config.py)
             'drill_edge_position': (x, y)
             'drill_gas': 0 - MAX_GAS (defined in config.py)
             'enemy_1_drill_angle': angle in rad
             'enemy_1_drill_position': (x, y)
             'enemy_1_drill_velocity': (n, m)
             'enemy_1_drill_mothership_position': (x, y)
             'enemy_1_drill_resources': 0 - 0
             'enemy_1_drill_touching_asteroid': 0 or 1
             'enemy_1_drill_touching_mothership': 0 or 1
             'enemy_1_drill_discharge_cooldown': 0 - COOLDOWN  (defined in config.py)
             'enemy_1_drill_edge_position': (x, y)
             'enemy_1_drill_gas': 0 - MAX_GAS (defined in config.py)
        :return: A Tuple containing the features you defined
        """
        # print("\n\n\nROL:", rightorleft(sensors['drill_edge_position'], sensors['drill_angle'], sensors['asteroid_position']))
        # print("ALIGN:", sensors['align_asteroid'])
        #
        # return (sensors['align_asteroid'],
        #         rightorleft(sensors['drill_edge_position'], sensors['drill_angle'], sensors['asteroid_position']),
        #         )

        gas = needGas(sensors['drill_gas'])
        align_a = sensors['align_asteroid']
        align_m = sensors['align_mothership']
        rol_a = rightorleft(sensors['drill_edge_position'], sensors['drill_angle'], sensors['asteroid_position'])
        rol_m = rightorleft(sensors['drill_edge_position'], sensors['drill_angle'], sensors['drill_mothership_position'])

        right = (rol_a and not gas and not align_a) or (rol_m and gas and not align_m)
        left = (not rol_a and not gas and not align_a) or (not rol_m and gas and not align_m)
        acel = (not gas and align_a) or (gas and align_m)

        return (right,
                left,
                acel,
                needDischarge(sensors['drill_discharge_cooldown'], sensors['enemy_1_drill_touching_asteroid']),
                sensors['drill_touching_asteroid']
                )


        # return (sensors['align_asteroid'],
        #         sensors['align_mothership'],
        #         sensors['drill_touching_asteroid'],
        #         needGas(sensors['drill_gas']),
        #         needDischarge(sensors['drill_discharge_cooldown'], sensors['enemy_1_drill_touching_mothership'], sensors['enemy_1_drill_touching_asteroid'])
        #         )

    def learn(self, weights: tuple):
        """
        IMPLEMENT YOUR LEARNING METHOD (i.e. YOUR LOCAL SEARCH ALGORITHM) HERE

        HINTS: You can call self.self.run_episode (see controller_interface.py) to evaluate a given set of weights.
               The variable self.episode shows the number of times self.run_episode method has been called

        :param weights: initial weights of the controller (either loaded from a file or generated randomly)
        :return: the best weights found by your learning algorithm, after the learning process is over
        """

        #return self.tabuSearch(weights)
        # return self.hillClimbing(weights)
        return self.geneticAlgorithm(weights)