import pymunk
from pygame.color import THECOLORS
from math import pi, cos, sin
from random import uniform, randint
import config
from copy import deepcopy

DRILL_COLLISION_TYPE = 1
ASTEROID_COLLISION_TYPE = 2
MOTHER_SHIP_COLLISION_TYPE = 3

TWO_PI = 2 * pi


class SpaceDrill(pymunk.Body):
    LEFT = 1
    RIGHT = 2
    ACCELERATE = 3
    DISCHARGE = 4
    NOTHING = 5

    MAX_GASOLINE = config.MAX_GAS

    _DISCHARGE_COOLDOWN = config.DISCHARGE_COOLDOWN
    _DISCHARGE_STUN_TIME = config.DISCHARGE_STUN_TIME
    _DEFAULT_VELOCITY = 5
    _DEFAULT_TURN = 0.2
    _MAX_VELOCITY = config.MAX_VELOCITY

    def __init__(self, x, y, space, ship_color, angle):
        mass = 1
        inertia = pymunk.inf
        super().__init__(mass, inertia)
        self.position = x, y
        self.angle = angle
        self.drill_shape = pymunk.Poly(self, ((-10, 20), (0, 85), (10, 20)))
        self.drill_shape.collision_type = DRILL_COLLISION_TYPE
        self.superior_cargo_shape = pymunk.Poly(self, ((-20, 0), (-10, 20), (10, 20), (18, 0)))
        self.inferior_cargo_shape = pymunk.Poly(self, ((-20, 0), (-24, -50), (23, -50), (18, 0)))
        space.add(self, self.drill_shape, self.inferior_cargo_shape, self.superior_cargo_shape)

        # Asteroid Joint Control
        self.asteroid_contact_point = 0, 0
        self.touching_asteroid = False
        self.jointed_with_asteroid = False
        self.asteroid_joint = None

        # Enemy mothership Joint Control
        self.mothership_contact_point = 0, 0
        self.touching_mothership = False
        self.jointed_with_mothership = False
        self.mothership_joint = None
        self.jointed_mothership_body = None

        # Game control
        self.resources = 0
        self.gasoline = self.MAX_GASOLINE
        self.discharge_cooldown = 0
        self.joint_blocked_time = 0
        self.discharge_activated = False

        # GUI feedback
        self.last_action = self.NOTHING

    def _discharge(self):
        for player in self.space.player_list:
            if player.drill is not self:
                player.drill.joint_blocked_time = self._DISCHARGE_STUN_TIME

        self.discharge_cooldown = self._DISCHARGE_COOLDOWN

    def frame_step(self, action):

        self.last_action = action
        self.discharge_activated = False

        if action == self.ACCELERATE:
            if self.gasoline > 0:
                self.apply_impulse_at_local_point((0, self._DEFAULT_VELOCITY), (0, 0))
                self.gasoline -= 1

        elif action == self.RIGHT:
            if not self.jointed_with_mothership and not self.jointed_with_asteroid:
                self.angle -= self._DEFAULT_TURN
        elif action == self.LEFT:
            if not self.jointed_with_mothership and not self.jointed_with_asteroid:
                self.angle += self._DEFAULT_TURN
        elif action == self.DISCHARGE and self.discharge_cooldown == 0:
            if self.resources > 0:
                self._discharge()
                self.discharge_activated = True
                self.resources *= .9
                self.resources = int(self.resources)

        """
        ATTACH MECHANICS ARE GOING TO BE IN A FUTURE VERSION OF THIS GAME
        elif action == self.ATTACH and self.joint_blocked_time == 0:
            self.add_or_remove_mothership_junction(self.space)
            self.add_or_remove_asteroid_junction(self.space.asteroid, self.space)
        """

        if self.angle < 0:
            self.angle = 2 * TWO_PI + self.angle
        if self.angle > TWO_PI:
            self.angle = self.angle - TWO_PI

        self.discharge_cooldown -= 1
        if self.discharge_cooldown < 0:
            self.discharge_cooldown = 0
        self.joint_blocked_time -= 1
        if self.joint_blocked_time < 0:
            self.joint_blocked_time = 0

        if self.velocity.length > self._MAX_VELOCITY + 1:
            self.velocity = self.velocity.normalized() * self._MAX_VELOCITY

    def add_or_remove_asteroid_junction(self, asteroid, space):
        if self.touching_asteroid is True and self.jointed_with_asteroid is False:
            self.asteroid_joint = pymunk.PinJoint(self, asteroid,
                                                  self.world_to_local(self.asteroid_contact_point)
                                                  , asteroid.world_to_local(self.asteroid_contact_point))
            space.add(self.asteroid_joint)
            self.jointed_with_asteroid = True
            pass
        elif self.jointed_with_asteroid is True:
            space.remove(self.asteroid_joint)
            self.jointed_with_asteroid = False
            self.velocity = space.asteroid.velocity
            pass

    def add_or_remove_mothership_junction(self, space):
        if self.touching_mothership is True and self.jointed_with_mothership is False:
            self.mothership_joint = pymunk.PinJoint(self, self.jointed_mothership_body,
                                                    self.world_to_local(self.mothership_contact_point)
                                                    , self.jointed_mothership_body.world_to_local(
                    self.mothership_contact_point))
            space.add(self.mothership_joint)
            self.jointed_with_mothership = True
            pass
        elif self.jointed_with_mothership is True:
            space.remove(self.mothership_joint)
            self.jointed_with_mothership = False
            pass

    @property
    def sensors(self):
        align = lambda x: 1 if 0.1 > x > -0.1 else 0
        sensors = {'asteroid_position': self.space.asteroid.position, 'asteroid_velocity': self.space.asteroid.velocity,
                   'asteroid_resources': self.space.asteroid.resources}
        enemy_number = 1

        for player in self.space.player_list:
            if player.drill is self:
                a = self.local_to_world((0, 40)) - player.drill.position
                b = self.space.asteroid.position - player.drill.position
                m = player.mothership.position - player.drill.position
                if self is self.space.drill_1:
                    m_e = self.space.mother_ship_2.position - player.drill.position
                else:
                    m_e = self.space.mother_ship_1.position - player.drill.position

                a = a.normalized()
                b = b.normalized()
                m = m.normalized()
                m_e = m_e.normalized()

                asteroid_angle = a.get_angle_between(b)
                mothership_angle = a.get_angle_between(m)
                enemy_mothership_angle = a.get_angle_between(m_e)

                sensors['align_asteroid'] = align(asteroid_angle)
                sensors['align_mothership'] = align(mothership_angle)
                sensors['align_enemy_mothership'] = align(enemy_mothership_angle)

                sensors['drill_angle'] = deepcopy(player.drill.angle)
                sensors['drill_position'] = deepcopy(player.drill.position)
                sensors['drill_velocity'] = deepcopy(player.drill.velocity)
                sensors['drill_mothership_position'] = deepcopy(player.mothership.position)
                sensors['drill_resources'] = deepcopy(player.drill.resources)
                sensors['drill_touching_asteroid'] = int(player.drill.touching_asteroid)
                sensors['drill_touching_mothership'] = int(player.drill.touching_mothership)
                sensors['drill_discharge_cooldown'] = deepcopy(player.drill.discharge_cooldown)
                sensors['drill_edge_position'] = deepcopy(player.drill.local_to_world((0, 40)))
                sensors['drill_gas'] = deepcopy(player.drill.gasoline)
            else:
                enemy_name = 'enemy_' + str(enemy_number) + '_'

                sensors[enemy_name + 'drill_angle'] = deepcopy(player.drill.angle)
                sensors[enemy_name + 'drill_position'] = deepcopy(player.drill.position)
                sensors[enemy_name + 'drill_velocity'] = deepcopy(player.drill.velocity)
                sensors[enemy_name + 'drill_mothership_position'] = deepcopy(player.mothership.position)
                sensors[enemy_name + 'drill_resources'] = deepcopy(player.drill.resources)
                sensors[enemy_name + 'drill_touching_asteroid'] = int(player.drill.touching_asteroid)
                sensors[enemy_name + 'drill_touching_mothership'] = int(player.drill.touching_mothership)
                sensors[enemy_name + 'drill_discharge_cooldown'] = deepcopy(player.drill.discharge_cooldown)
                sensors[enemy_name + 'drill_edge_position'] = deepcopy(player.drill.local_to_world((0, 40)))
                sensors[enemy_name + 'drill_gas'] = deepcopy(player.drill.gasoline)

        return sensors


class MotherShip(pymunk.Body):
    def __init__(self, space, color, owner, position, angle):
        mass = 100
        inertia = pymunk.inf
        super().__init__(mass, inertia, body_type=pymunk.Body.KINEMATIC)
        self.position = position
        self.mother_ship_shape = pymunk.Poly(self, ((-85, -45), (-95, 0), (-85, 45), (-62, 79), (0, 90),
                                                    (62, 79), (85, 45), (95, 0), (85, -45), (0, -70)))
        self.mother_ship_shape.elasticity = 0
        self.mother_ship_shape.color = color
        self.mother_ship_shape.collision_type = MOTHER_SHIP_COLLISION_TYPE
        self.angle = angle
        self.owner = owner

        space.add(self, self.mother_ship_shape)

        self.resources = 0


class Asteroid(pymunk.Body):
    ASTEROID_POLY_POINTS = 100
    NOISE_OFFSET = 10
    ASTEROID_MASS = config.ASTEROID_MASS

    def __init__(self, pos_x, pos_y, radius_x, radius_y, space):
        mass = self.ASTEROID_MASS
        inertia = pymunk.inf
        super().__init__(mass, inertia, body_type=pymunk.Body.DYNAMIC)
        self.position = pos_x, pos_y
        self.rad_x, self.rad_y = radius_x, radius_y
        self.asteroid_shape = pymunk.Poly(self, self._generate_asteroid_poly())
        self.asteroid_shape.collision_type = ASTEROID_COLLISION_TYPE
        self.asteroid_shape.color = THECOLORS['gray100']
        self.set_for_destruction = False

        space.add(self, self.asteroid_shape)

        self.resources = radius_y * radius_x / 10
        self.resources_initial = self.resources

    def _generate_asteroid_poly(self):
        poly = []
        angle_between_points = (2 * pi) / self.ASTEROID_POLY_POINTS

        for poly_point_number in range(0, self.ASTEROID_POLY_POINTS):
            angle = angle_between_points * poly_point_number
            offset = uniform(-self.NOISE_OFFSET, self.NOISE_OFFSET)
            x = cos(angle) * (self.rad_x + offset)
            y = sin(angle) * (self.rad_y + offset)
            poly.append((x, y))

        return poly


def asteroid_set_joint(arbiter, space, data):
    arbiter.shapes[0].body.asteroid_contact_point = arbiter.contact_point_set.points[0].point_a
    arbiter.shapes[0].body.touching_asteroid = True
    return True


def asteroid_drill_remove_joint(arbiter, space, data):
    arbiter.shapes[0].body.touching_asteroid = False
    return True


def asteroid_drill_point_update(arbiter, space, data):
    arbiter.shapes[0].body.asteroid_contact_point = arbiter.contact_point_set.points[0].point_a
    return True


def drill_mother_ship_set_joint(arbiter, space, data):
    arbiter.shapes[0].body.mothership_contact_point = arbiter.contact_point_set.points[0].point_a
    arbiter.shapes[0].body.touching_mothership = True
    arbiter.shapes[0].body.jointed_mothership_body = arbiter.shapes[1].body
    return True


def drill_mother_ship_remove_joint(arbiter, space, data):
    arbiter.shapes[0].body.touching_mothership = False
    arbiter.shapes[0].body.velocity = 0, 0
    return True


def drill_mother_ship_point_update(arbiter, space, data):
    arbiter.shapes[0].body.mothership_contact_point = arbiter.contact_point_set.points[0].point_a
    return True


def asteroid_mother_ship_collision(arbiter, space, data):
    space.asteroid.set_for_destruction = True
    return True
